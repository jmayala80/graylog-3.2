# Graylog 3.2
Graylog es una plataforma poderosa que permite una fácil gestión de registros de datos estructurados y no estructurados junto con aplicaciones de depuración. Se basa en Elasticsearch y MongoDB.
Cuenta con un servidor principal, que recibe datos de sus clientes instalados en diferentes servidores, y una interfaz web, que visualiza los datos y permite trabajar con registros agregados por el servidor principal.


# Configuración mínima
Esta es una configuración de Graylog mínima que se puede utilizar para configuraciones más pequeñas, no críticas o de prueba. Ninguno de los componentes es redundante y son fáciles y rápidos de configurar.

![alt text](img/architec_small_setup.png "Architec Small Setup")

# .env
En el archivo: `.env`, se setean todas la variables que son utilizadas en el: `docker-compose.yml`.
Las variables que definen son: hostname de los containers, rutas donde persisten datos y configuraciones, puertos, parametros de java, entre otras configuraciones.

Antes de levantar los container es necesario modificar la variable: `${DOCKER_GRAYLOG}`, por el path correspondiente al proyecto, por ejemplo: `/home/dockerusr/graylog`

Para verificar configuración:
```
$ docker-compose -f docker-compose.yml config
```

# docker-compose.yml
Toma variables y configuraciones del archivo: .env, prácticamente no es necesario realizar cambios en este archivo.

Para levantar los servicios ejecutar:
```
$ docker-compose up -d
```

# Crear estructura de directorios
Crear la siguiente estructura de dicrectorios y asignar permisos, parados en el directorio del proyecto:
```
$ mkdir -p graylog/elasticsearch/data
$ mkdir -p graylog/mongo/data
$ mkdir -p graylog/graylog/journal
$ mkdir -p graylog/nginx/conf

$ chmod -R 777 graylog/
```
# nginx: default.conf
En el archivo de configuración: modificar la variable: `${SERVER_NAME}`, por la ip o el nombre del servidor y copiar en la ruta: `graylog/nginx/conf`
```
$ cp default.conf graylog/nginx/conf
```
# Troubleshooting
 - https://docs.graylog.org/en/3.3/pages/installation/docker.html?highlight=docker#troubleshooting

# Timezone
Hay 3 configuraciones de zona horaria:
 1. la de su Usuario
 2. la del Explorador de Internet
 3. la de Graylog Server
 
> Realizo esta aclaración ya que esto se torna confuso para el usuario. Se puede ver esto en: System/Overview -> Time configuration,   
> También se puede setear la zona horario para el usuario, desde: Edit Profile -> Time Zone  


# Productos
 - https://docs.graylog.org/en/3.3/pages/installation/docker.html?highlight=docker#docker
 - https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
 - https://docs.mongodb.com/manual/
 - https://www.nginx.com/
 - https://github.com/mongo-express/mongo-express
 - https://www.redhat.com/sysadmin/cerebro-webui-elk-cluster